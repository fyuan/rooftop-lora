sudo add-apt-repository -y ppa:ettusresearch/uhd
sudo add-apt-repository -y ppa:johnsond-u/sdr
sudo apt-get update

for thing in $*
do
    case $thing in
        gnuradio)
            sudo DEBIAN_FRONTEND=noninteractive apt-get install -y gnuradio python3-gi gobject-introspection gir1.2-gtk-3.0 python3-cairo python3-gi-cairo
            ;;

        srslte)
            sudo DEBIAN_FRONTEND=noninteractive apt-get install -y srslte
            sudo apt-get update;
        	sudo apt-get install python3-pip;
            ;;
    esac
done

sudo apt-get -y install cmake
sudo apt-get -y install swig
cd /usr/lib/x86_64-linux-gnu/
sudo ln -s liborc-0.4.so.0.28.0 liborc-0.4.so

# mkdir gr-lora_sdr

#
# This sets the X11 geometry for uhd_fft. It does not take a --geometry arg.
#
# GENIUSER=`geni-get user_urn | awk -F+ '{print $4}'`
# HOMEDIR="/users/$GENIUSER"
# cd $HOMEDIR
cd ~
git clone https://gitlab.flux.utah.edu/fyuan/gr_lora_sdr_powder.git
cd ~/gr_lora_sdr_powder
mkdir build
cd build
cmake .. -DCMAKE_INSTALL_PREFIX=/usr
make
sudo make install
sudo ldconfig

sudo sysctl -w net.core.rmem_max=24862979
sudo sysctl -w net.core.wmem_max=24862979

sudo rm /usr/bin/uhd_rx_cfile
chmod +x /local/repository/uhd_rx_cfile
sudo mv /local/repository/uhd_rx_cfile /usr/bin/

chmod +x /local/repository/uhd_siggen_45sec
sudo mv /local/repository/uhd_siggen_45sec /usr/bin/

chmod +x /local/repository/uhd_siggen_90sec
sudo mv /local/repository/uhd_siggen_90sec /usr/bin/

chmod +x /local/repository/iq_to_power.py
sudo mv /local/repository/iq_to_power.py /usr/bin/

chmod +x /local/repository/OFDM_TX.py
sudo mv /local/repository/OFDM_TX.py /usr/bin/

chmod +x /local/repository/OFDM_RX.py
sudo mv /local/repository/OFDM_RX.py /usr/bin/

sudo ed /etc/sysctl.conf << "EDEND"
a
net.core.rmem_max=24862979
net.core.wmem_max=24862979
.
w
EDEND

#sudo "/usr/lib/uhd/utils/uhd_images_downloader.py -t x310"
